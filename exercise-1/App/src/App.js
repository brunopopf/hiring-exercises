import React from 'react';
import { ApolloProvider } from '@apollo/client';
import { ApolloClient, InMemoryCache } from '@apollo/client';
import ExerciseComponent from './components/ExerciseComponent';

const client = new ApolloClient({
  uri: 'https://graphqlzero.almansi.me/api',
  cache: new InMemoryCache()
});


function App() {
  return (
      <ApolloProvider client={client}>
        <ExerciseComponent />
      </ApolloProvider>  
  );
}

export default App;

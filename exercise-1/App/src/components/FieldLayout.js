import React from 'react';

/**
 *  "title: value" layout
 */
export const FieldLayout = (props) => {
    return (
        <>
            {props.isFirst
                ? <FirstTitle>{props.title}</FirstTitle>
                : <Title>{props.title}</Title>}
            <Value>{props.value}</Value>
        </>
    )
}

//A bit unnecessary -> Following the requirement as much components is better
export const FirstTitle = (props) => {
    return <p className={"pt-4 text-base font-bold flex items-center justify-center lg:justify-start " + props.extraClass}>{props.children}:</p>
}
export const Title = ({ children }) => {
    return <FirstTitle extraClass="mt-4">{children}</FirstTitle>
}
export const Value = ({ children }) => {
    return <p className="pt-2 text-gray-600 text-xs lg:text-sm flex items-center justify-center lg:justify-start">{children}</p>
}
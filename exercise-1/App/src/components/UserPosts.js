import React from 'react';
import { Value, Title } from './FieldLayout';

/**
 * Posts of a given user
 */
const UserPosts = ({ user }) => {
    return <div className="w-full lg:w-2/5 mx-6 lg:mx-0 h-screen py-12">
        <div className="w-full rounded-lg lg:rounded-r-lg lg:rounded-l-none shadow-2xl bg-white opacity-75 mx-6 lg:mx-0 px-12 h-full overflow-auto">
            <Title>{user.name}'s Posts</Title>
            {user.posts.data.map(p => {
                return <Value>Title: {p.title}</Value>;
            })}
        </div>
    </div>
}

export default UserPosts

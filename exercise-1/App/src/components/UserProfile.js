import React from 'react';
import { FieldLayout } from './FieldLayout';

/**
 * User profile's fields
 */
const UserProfile = ({ user }) => {
    //address value
    const address = user.address.street + " " + user.address.suite + " " + user.address.city;

    return <div id="profile" className="w-full lg:w-3/5 rounded-lg lg:rounded-l-lg lg:rounded-r-none shadow-2xl bg-white opacity-75 mx-6 lg:mx-0">
        <div className="p-4 md:p-12 text-center lg:text-left">
            <UserName>{user.name}</UserName>
            <FieldLayout title="Adress" value={address} isFirst={true} />
            <FieldLayout title="Email" value={user.email} />
            <FieldLayout title="Phone" value={user.phone} />
            <FieldLayout title="Company" value={user.company.name} />
        </div>
    </div>
}

const UserName = ({ children }) => {
    return <>
        <h1 className="text-3xl font-bold pt-8 lg:pt-0">{children}</h1>
        <div className="mx-auto lg:mx-0 w-4/5 pt-3 border-b-2 border-teal-500 opacity-25"></div>
    </>
}

export default UserProfile
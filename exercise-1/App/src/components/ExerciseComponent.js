import React, { useEffect } from 'react';
import { useQuery, gql } from '@apollo/client';
import Loading from './Loading'
import ErrorComponent from './ErrorComponent'
import UserProfile from './UserProfile'
import UserPosts from './UserPosts'

const GET_USER = gql`
query {
    user(id: 1) {
      name
      email
      phone
      company{name}
      address {
        street
        suite
        city
      }  
       posts (
        options:{
          paginate:{
            page: 1
            limit:10
          }
        })  {    
        data {
          title        
        }
      }
    }
  }
`;

const ExerciseComponent = () => {

    //Set body styles
    useEffect(() => {
        document.body.className = "font-sans antialiased text-gray-900 leading-normal tracking-wider bg-cover p12";
        document.body.style.backgroundImage = "url('https://source.unsplash.com/1L71sPT5XKc')";
        return () => {
            document.body.className = '';
            document.body.style.backgroundImage = ''
        }
    });

    //Loading user
    const { loading, error, data } = useQuery(GET_USER);
    if (loading) return <Loading />;
    else if (error) return <ErrorComponent error={error} />;
    else console.log(data.user)

    //Layout
    return (
            <div className="max-w-4xl flex items-center h-auto lg:h-screen flex-wrap mx-auto my-32 lg:my-0">
                <UserProfile user={data.user} />
                <UserPosts user={data.user} />
            </div>        
    )
}

export default ExerciseComponent
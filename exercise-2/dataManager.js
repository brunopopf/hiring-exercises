const fs = require('fs');
const PATH = './data/';

module.exports =
{
    readFile,
    writeFile
}

//PRE: Local json file's name
//POS: Return json
function readFile(file) {
    let rawdata = fs.readFileSync(PATH + file);
    return JSON.parse(rawdata);
}

//PRE: Local json file's name
//POS: Save json data in file
function writeFile(file, data) {
    fs.writeFile(PATH + file, JSON.stringify(data), error => {
        error
            ? console.log("Error saving " + file + ": " + error)
            : console.log("Succes saving " + file);
    });
}
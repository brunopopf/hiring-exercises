
const cm = require('./customerManager');
const dm = require('./dataManager');

const handler = async (country) => {
  try {
    let finalCustomers = []

    //Get Stripe format customers filtered by country
    let customers = cm.getByCountryStripeFormat(country);

    //Post costumers in Stripe
    cm.saveMultipleToStripe(customers)
      .then(res => {
        finalCustomers = [...res];
      }).catch(res => {
        finalCustomers = [...res.finalCustomers];
        // console.log(res.errors);
      }).finally(() => {
        //Result -> save & log
        dm.writeFile('final-customers.json', finalCustomers);
        console.log(finalCustomers);
      });

    /* add code above this line */ //-> I did not respect this comment but I am happy with my solution :)
    // console.log(finalCustomers)

  } catch (e) {
    throw e
  }

}

//run
handler("Spain");

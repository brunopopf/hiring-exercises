const Stripe = require('stripe');
const STRIPE_TEST_SECRET_KEY = 'rk_test_51EDa6GIrkMUcWfFwXon9vsJYpTqX2eTqbINAUf4fZC7ivToWv59cAPoHdYhmszwL9ZKWJtUUbaCcHtpjl6rWlXLP00C1dcAoUR'
const stripe = Stripe(STRIPE_TEST_SECRET_KEY);
const dm = require('./dataManager');
const cm = require('./countryManager');

module.exports =
{
    // getAll,
    // getByCountry,
    getByCountryStripeFormat,
    // saveToStripe,
    saveMultipleToStripe,
}

//POS: Return all customers
function getAll() {
    return customers = dm.readFile('customers.json');
}

//PRE: Country name
//Pos: Return filtered customers in Stripe format
function getByCountryStripeFormat(country) {
    const countryCode = cm.getCountryCode(country);
    return getAll().reduce((customers, c) => {
        //filter
        if (c.country === country) {
            //add it in stripe format
            customers.push({
                email: c.email,
                name: c.first_name + " " + c.last_name,
                address: {
                    //Note -> I added this value as: -is required by Stripe to add a country value -is available in customer.json.
                    line1: c.address_line_1,
                    country: countryCode
                },
            })
        }
        return customers;
    }, [])
}

//PRE: Stripe format customer
//POS: Create Stripe customer
async function saveToStripe(customer) {
    return customer = await stripe.customers.create(customer);
}

//PRE: Array of customers in Stripe format
//POS: Create multiple Stripe customers. Return a Promise: succes-> finalCustomers. error-> finalCustomers, errors
function saveMultipleToStripe(customers) {
    return new Promise((resolve, reject) => {
        //auxs
        let count = customers.length;
        let finished = 0;
        let finalCustomers = [];
        let errors = [];
        //save customers
        customers.forEach(c => {
            saveToStripe(c)
                .then(res => {
                    //add saved customer
                    finalCustomers.push({
                        email: res.email,
                        customerId: res.id,
                        country: res.address.country,
                    })
                }).catch(error => {
                    //error
                    errors.push(error);
                }).finally(() => {
                    //finish promise
                    finished++;
                    if (finished === count)
                        errors.length > 0 ? reject({ finalCustomers: finalCustomers, errors: errors }) : resolve(finalCustomers)
                })
        })
    });
}

/*Notes

 -> I could get the customers by country and implement a "pure" funtion 
 to convert the customer in Stripe format, as it was recomended.

 -> I decided to do it in the same function to avoid unnecessary iterations.
 
 -> Considering that all customers are required from the same country, the
    implementation of a convert funtion without converting the country code
    it wouldn't make much sense.

*/

//PRE: Country name
//Pos: Return filtered customers 
function getByCountry(country) {
    return getAll().filter(c => c.country === country);
}

//PRE: Customer
//POS: Stripe customer format
function toStripe(customer) {
    return {
        email: customer.email,
        name: customer.first_name + " " + customer.last_name,
        address: {
            line1: customer.address_line_1,
            //...getCountryCode or add it afterwards
        }
    }
}





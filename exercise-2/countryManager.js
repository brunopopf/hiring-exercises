const dm = require('./dataManager');

module.exports =
{
    getAll,
    getCountryCode,
}

//POS: Return all countries
function getAll(){
    return countries = dm.readFile('countries-ISO3166.json');
}

//PRE: Country name
//Pos: Return country code
function getCountryCode(country){
    const countries = getAll();
    return Object.keys(countries).find(key => countries[key] === country)
}
